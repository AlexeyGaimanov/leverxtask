from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from simpletaskmanager.models import User, Task, Project


class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.CharField(source='get_absolute_url', read_only=True)
    projects = serializers.PrimaryKeyRelatedField(many=True, queryset=Project.objects.all(),
                                                  required=False, default=None, allow_null=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'password', 'first_name', 'last_name',
                  'role', 'email', 'department', 'description', 'projects')
        extra_kwargs = {
            'password': {
                'write_only': True
            },
            'role': {
                'read_only': True
            }
        }

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data.pop('username'),
            password=validated_data.pop('password'),
            email=validated_data.pop('email')
        )
        return serializers.ModelSerializer.update(self, user, validated_data)

    def update(self, instance, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        return super(UserSerializer, self).update(instance, validated_data)


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.CharField(source='get_absolute_url', read_only=True)
    project = serializers.PrimaryKeyRelatedField(queryset=Project.objects.all(),
                                                 allow_null=True, default=None)
    executor = serializers.PrimaryKeyRelatedField(queryset=User.objects.filter(role=User.DEVELOPER),
                                                  required=True)
    creator = serializers.ReadOnlyField(source='creator.username')

    class Meta:
        model = Task
        fields = ('id', 'url', 'title', 'description', 'created_at', 'deadline',
                  'project', 'executor', 'creator', 'is_done')


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.CharField(source='get_absolute_url', read_only=True)
    tasks = TaskSerializer(many=True, read_only=True)
    participants = UserSerializer(many=True, read_only=True)

    class Meta:
        model = Project
        fields = ('url', 'id', 'title', 'description', 'created_at', 'tasks', 'participants')
