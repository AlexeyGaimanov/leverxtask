from django.conf.urls import url, include
from api.views import TaskViewSet, ProjectViewSet, DeveloperViewSet, ManagerViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'managers', ManagerViewSet)
router.register(r'developers', DeveloperViewSet)
router.register(r'tasks', TaskViewSet)
router.register(r'projects', ProjectViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]
