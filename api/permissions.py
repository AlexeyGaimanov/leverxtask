from rest_framework import permissions
from simpletaskmanager.models import User


class IsManagerOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        return (
            (request.method in permissions.SAFE_METHODS and request.user.is_authenticated) or
            (request.user.is_authenticated and request.user.role == User.MANAGER)
        )


class IsGeneralManagerOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        return (
            (request.method in permissions.SAFE_METHODS and request.user.is_authenticated) or
            (request.user.is_authenticated and request.user.is_superuser and
             request.user.role == User.MANAGER)
        )
