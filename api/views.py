from rest_framework import viewsets
from simpletaskmanager.models import Task, Project, User
from api.serializers import UserSerializer, TaskSerializer, ProjectSerializer
from api.permissions import IsManagerOrReadOnly, IsGeneralManagerOrReadOnly


class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = (IsManagerOrReadOnly,)
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class DeveloperViewSet(viewsets.ModelViewSet):
    permission_classes = (IsManagerOrReadOnly,)
    queryset = User.objects.filter(role=User.DEVELOPER)
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        serializer.save(role=User.DEVELOPER)


class ManagerViewSet(viewsets.ModelViewSet):
    permission_classes = (IsGeneralManagerOrReadOnly,)
    queryset = User.objects.filter(role=User.MANAGER)
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        serializer.save(role=User.MANAGER)


class ProjectViewSet(viewsets.ModelViewSet):
    permission_classes = (IsManagerOrReadOnly,)
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def perform_create(self, serializer):
        project = serializer.save()
        self.request.user.projects.add(project)
