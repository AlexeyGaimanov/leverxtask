from django.conf.urls import url, include
from django.shortcuts import reverse
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', include('api.urls', namespace='api')),
]
