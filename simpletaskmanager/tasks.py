from celery.task import periodic_task, task
from datetime import timedelta
from simpletaskmanager.models import User
from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from datetime import datetime


TASKS_EMAIL_SENDING_TIME = 12


@periodic_task(run_every=timedelta(hours=TASKS_EMAIL_SENDING_TIME),
               name="mass_email_sending", ignore_result=True)
def mass_email_sending_task():
    users = User.objects.all()
    for user in users:
        if user.role == User.MANAGER:
            tasks = user.created_tasks.all().filter(is_done=False)
            if tasks:
                send_tasks_to_user(user, tasks)

        elif user.role == User.DEVELOPER:
            tasks = user.received_tasks.all().filter(is_done=False)
            if tasks:
                send_tasks_to_user(user, tasks)


@task(name="send_tasks_to_user")
def send_tasks_to_user(user, tasks):
    message = render_to_string('email_notifications/tasks_email_tpl.html',
                               {
                                   'tasks': tasks.filter(deadline__gte=datetime.now()),
                                   'expired_tasks': tasks.filter(deadline__lt=datetime.now()),
                                   'user': user
                               })
    letter = EmailMessage('Tasks for today...', message, settings.EMAIL_HOST_USER, [user.email])
    letter.content_subtype = "html"
    letter.send()
