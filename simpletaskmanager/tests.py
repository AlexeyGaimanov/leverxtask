from django.test import TestCase
from simpletaskmanager.models import User, Task, Project
import datetime


class CreateAndGetUserTestCase(TestCase):

    def setUp(self):
        self.new_user = User.objects.create(username='alexey',
                                            password='alexeyalexey',
                                            email='alexeygaimanoff@yandex.ru',
                                            first_name='Alexey',
                                            last_name='Gaimanov',
                                            department='WebDepartment')
        self.new_user.save()
        self.user = User.objects.get(username='alexey')

    def test(self):
        self.assertEqual(self.user, self.new_user)
        self.assertEqual(self.user.role, User.MANAGER)
        self.assertFalse(self.user.description)


class CreateAndGetTaskTestCase(TestCase):

    def setUp(self):
        self.new_task = Task.objects.create(title='Task One',
                                            deadline=datetime.datetime(2017, 1, 10, 8, 0, 0))
        self.new_task.save()
        self.task = Task.objects.get(title='Task One')

    def test(self):
        self.assertEqual(self.task, self.new_task)
        self.assertFalse(self.task.description)
        self.assertTrue(self.task.created_at)
        self.assertEqual(self.task.creator, None)
        self.assertEqual(self.task.executor, None)
        self.assertFalse(self.task.is_done)


class CreateAndGetProjectTestCase(TestCase):

    def setUp(self):
        self.new_project = Project.objects.create(title='Project One')
        self.new_project.save()
        self.project = Project.objects.get(title='Project One')

    def test(self):
        self.assertEqual(self.project, self.new_project)
        self.assertFalse(self.project.description)
        self.assertTrue(self.project.created_at)
