from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from datetime import datetime


class Project(models.Model):
    title = models.CharField(verbose_name='project title', db_index=True,
                             max_length=50, unique=True)
    description = models.TextField(verbose_name='task description', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='date and time of start the project',
                                      auto_now_add=True)

    class Meta:
        db_table = 'project'
        ordering = ['-created_at']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return '/projects/%s' % self.id


class Task(models.Model):
    title = models.CharField(verbose_name='task title', max_length=50)
    description = models.TextField(verbose_name='task description',
                                   max_length=1000, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='date and time of creation the task',
                                      auto_now_add=True)
    deadline = models.DateTimeField(verbose_name='due date and time of the task')
    project = models.ForeignKey(Project, related_name='tasks', null=True, blank=True)
    executor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                 null=True, blank=True, related_name='received_tasks')
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                null=True, blank=True, related_name='created_tasks',
                                editable=False)
    is_done = models.BooleanField(verbose_name='the task is done', default=False)

    class Meta:
        db_table = 'task'
        ordering = ['-created_at']
        unique_together = ['title', 'created_at', 'creator']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return '/tasks/%s' % self.id


class User(AbstractUser):
    DEVELOPER = 'Developer'
    MANAGER = 'Manager'
    role_choice = (
        (DEVELOPER, 'Developer'),
        (MANAGER, 'Manager')
    )

    department = models.CharField(max_length=30, blank=True, null=True)
    description = models.TextField(max_length=300, blank=True, null=True)
    projects = models.ManyToManyField('Project', related_name='participants', blank=True)
    role = models.CharField(max_length=20, choices=role_choice, default=MANAGER)

    class Meta:
        db_table = 'user'
        ordering = ['department']

    objects = UserManager()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def is_developer(self):
        return self.role == self.DEVELOPER

    def is_manager(self):
        return self.role == self.MANAGER

    def get_absolute_url(self):
        return ('/managers/%s' if self.role == User.MANAGER else 'developers/%s') % self.id
