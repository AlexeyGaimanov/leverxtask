from django.apps import AppConfig


class SimpletaskmanagerConfig(AppConfig):
    name = 'simpletaskmanager'
