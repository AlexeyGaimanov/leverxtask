from django.contrib import admin
from simpletaskmanager.models import Task, Project, User


class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_at', 'deadline', 'is_done')


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'first_name', 'last_name',
                    'department', 'role', 'email')


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_at')

admin.site.register(Task, TaskAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(User, UserAdmin)
